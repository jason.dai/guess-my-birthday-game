from random import randint

#Greeting to ask for name
name = input("Hi! What is your name? ")

for guess_number in range(5):
    guess_number = guess_number + 1
    #Guess 1

    #Guessing a random month
    guess_month = randint(1,12)
    #Guessing a random year
    guess_year = randint(1924, 2004)

    print("Guess", guess_number, ":", name, "were you born on",
    guess_month, "/", guess_year,"?")

    response = input("yes or no? ")

    if response == "yes":
        print("I knew it!")
        break
    elif guess_number == 5:
        print("Drat! Let me try again!")
    else:
        print("I have other things to do. Goodbye.")
